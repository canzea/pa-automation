
This application is one that runs on a desktop and performs tasks on behalf of the user.  The technology used is:

- tesseract (https://github.com/tesseract-ocr/tesseract)
- sikuli (https://github.com/RaiMan/SikuliX-2014)
- opencv (http://opencv.org/)

The application securely communicates with a cloud service to exchange information and provide next tasks to be performed.


# Getting Started


# Testing

    export CPATH=/usr/local/Cellar/tesseract/3.04.00/include:/usr/local/Cellar/leptonica/1.72/include
    export LIBRARY_PATH=/usr/local/Cellar/tesseract/3.04.00/lib:/usr/local/Cellar/leptonica/1.72/lib
    
    g++ tesseract.cc -ltesseract -llept -o sample

