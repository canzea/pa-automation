#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

int main()
{
  char *outText;

  printf("Version : %s\n\n", tesseract::TessBaseAPI::Version());

  tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
  // Initialize tesseract-ocr with English, without specifying tessdata path
  Pix *image = pixRead("examples/example_002.png");
  
  api->Init(NULL, "eng");
  api->SetImage(image);
  api->SetPageSegMode(tesseract::PSM_AUTO_OSD);
  api->Recognize(NULL);
  tesseract::ResultIterator* ri = api->GetIterator();
  tesseract::PageIteratorLevel level = tesseract::RIL_WORD;
  if (ri != 0) {
    do {
      const char* word = ri->GetUTF8Text(level);
      float conf = ri->Confidence(level);
      int x1, y1, x2, y2;
      ri->BoundingBox(level, &x1, &y1, &x2, &y2);
      printf("word: '%s';  \tconf: %.2f; BoundingBox: %d,%d,%d,%d;\n",
               word, conf, x1, y1, x2, y2);
//      delete[] word;
    } while (ri->Next(level));
  }

  printf("All done");
  // Destroy used object and release memory
  api->End();
  //delete [] outText;
  pixDestroy(&image);

  return 0;
}
